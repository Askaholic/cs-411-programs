use std::cmp::Ordering::*;


fn contig_sum(list: &[i32]) -> u32 {
    contig_sum_index(list).0
}

fn contig_sum_index(list: &[i32]) -> (u32, u32, u32, i32) {
    use std::cmp::max;

    let len = list.len();
    match len {
        0 => (0, 0, 0, 0),
        1 => match list[0].cmp(&0) {
            Greater | Equal => {
                let l = list[0] as u32;
                (l, l, l, list[0])
            },
            Less => (0, 0, 0, list[0])
        },
        _ => {
            let (head_a, head_b, head_c, head_d) = contig_sum_index(&list[0..len/2]);
            let (tail_a, tail_b, tail_c, tail_d) = contig_sum_index(&list[len/2..len]);
            (
                max(max(head_a, tail_a), head_c + tail_b),
                max(head_b, max(head_d + tail_b as i32, 0) as u32),
                max(tail_c, max(tail_d + head_c as i32, 0) as u32),
                head_d + tail_d
            )
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;


    macro_rules! test {
        ($fn:expr, $ans:expr, $txt:tt) => {
            println!($txt);
            assert_eq!($fn, $ans);
        };
    }

    #[test] fn types() {
        let _: u32 = contig_sum(&vec![0]);
    }

    #[test] fn small() {
        test!(contig_sum(&vec![]), 0, "Empty sequence");
        test!(contig_sum(&vec![5]), 5, "Size 1, positive");
        test!(contig_sum(&vec![-3]), 0, "Size 1, negative");
        test!(contig_sum(&vec![0]), 0, "Size 1, zero");
        test!(contig_sum(&vec![5, 3]), 8, "Size 2, pos pos");
        test!(contig_sum(&vec![5, -4]), 5, "Size 2, pos neg");
        test!(contig_sum(&vec![-5, 3]), 3, "Size 2, neg pos");
        test!(contig_sum(&vec![-5, -6]), 0, "Size 2, neg neg");
        test!(contig_sum(&vec![6, -5, 2]), 6, "Size 3, #1");
        test!(contig_sum(&vec![1, -5, 7]), 7, "Size 3, #2");
        test!(contig_sum(&vec![6, -5, 7]), 8, "Size 3, #3");
        test!(contig_sum(&vec![6, -5, 0, 7, 11, -6, -1, 9, 12, -7, 5, 8, 2, 4, 10, -4, 3, 1, -2, -3]),
            55, "Size 20, #1");
        test!(contig_sum(&vec![5, -2, 1, 12, 10, -1, 3, 4, 8, 11, 6, -5, -7, -3, 9, 7, 0, -4, -6, 2]),
            58, "Size 20, #2");
        test!(contig_sum(&vec![7, -1, 8, 0, 5, 1, 10, 2, -7, 3, -5, 11, 12, -4, 9, 6, 4, -3, -2, -6]),
            61, "Size 20, #3");
    }

    #[test] fn large() {
        {
            const N: usize = 100000;
            let mut v = Vec::with_capacity(N * 7);
            for _ in 0..N {
                v.push(2);
                v.push(-3);
            }
            for _ in 0..N {
                v.push(5);
                v.push(-4);
                v.push(5);
            }
            for _ in 0..N {
                v.push(-2);
                v.push(1);
            }
            test!(contig_sum(&v), 6 * N as u32, "Large #1");
        }

        {
            const N: usize = 100000;
            let mut v = Vec::with_capacity(N * 6);
            for _ in 0..N {
                v.push(2);
                v.push(-3);
            }
            for _ in 0..N {
                v.push(6);
                v.push(-4);
            }
            for _ in 0..N {
                v.push(-2);
                v.push(1);
            }
            test!(contig_sum(&v), (2 * N + 4) as u32, "Large #2");
        }

        {
            const N: usize = 100000;
            let mut v = Vec::with_capacity(N * 3);
            for _ in 0..N {
                v.push(5);
            }
            for _ in 0..N {
                v.push(-6);
            }
            for _ in 0..N {
                v.push(7);
            }
            test!(contig_sum(&v), 7 * N as u32, "Large #3");
        }

        {
            const N: usize = 100000;
            let mut v = Vec::with_capacity(N * 3);
            for _ in 0..N {
                v.push(8);
            }
            for _ in 0..N {
                v.push(-6);
            }
            for _ in 0..N {
                v.push(7);
            }
            test!(contig_sum(&v), 9 * N as u32, "Large #4");
        }
    }
}
