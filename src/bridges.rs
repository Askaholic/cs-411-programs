use std::cmp::Ordering::*;


#[derive(Clone, PartialEq, Debug)]
pub struct Bridge {
    west: usize,
    east: usize,
    toll: i32
}
impl Bridge {
    pub fn new(west: usize, east: usize, toll: i32) -> Bridge {
        Bridge { west, east, toll }
    }

    pub fn crosses(&self, other: &Bridge) -> bool {
        match (self.west.cmp(&other.west), self.east.cmp(&other.east)) {
            (Less, Less) | (Greater, Greater) => false,
            _ => true
        }
    }
}

pub fn build(_w: usize, _e: usize, bridges: &[Bridge]) -> (i32, Vec<Bridge>) {
    build_recurse(bridges)
}

fn build_recurse(bridges: &[Bridge]) -> (i32, Vec<Bridge>) {
    if bridges.len() == 0 { return (0, vec![]); }

    let (toll_without, bridges_without) =
        build_recurse(&bridges[1..]);

    let (mut toll_with, mut bridges_with) =
        build_recurse(non_crossing(&bridges[0], &bridges[1..]).as_slice() );
    toll_with += bridges[0].toll;

    // Compare
    match toll_with.cmp(&toll_without) {
        Less => (toll_without, bridges_without),
        _ => {
            bridges_with.push(bridges[0].clone());
            (toll_with, bridges_with)
        }
    }
}

#[inline(always)]
fn non_crossing(bridge: &Bridge, bridges: &[Bridge]) -> Vec<Bridge> {
    let mut vec = vec![];
    for other in bridges.iter() {
        if ! bridge.crosses(other) {
            vec.push(other.clone());
        }
    }
    vec
}

#[cfg(test)]
mod tests {
    use super::*;

    use test;
    use test::Bencher;
    use rand;
    use rand::Rng;

    #[bench] fn large_examples(b: &mut Bencher) {
        let mut r = rand::thread_rng();

        let mut bs: Vec<Bridge> = vec![];
        let w = 40;
        let e = 40;
        let num_bridges = 100;

        for _ in 0..num_bridges {
            bs.push(Bridge::new(r.gen_range(0, w), r.gen_range(0, e), r.gen_range(0, 20)));
        }
        b.iter(|| {
            let bs = test::black_box(&bs);
            build(w, e, &bs)
        })
    }

    fn test_build_single(w: usize, e: usize, bridges: &[Bridge], ans: i32, msg: &'static str) {
        let (retval, _) = build(w, e, bridges);
        println!("{}... {}.", msg, if retval == ans { "ok" } else { "failed" });
        assert_eq!(retval, ans);
    }

    #[test] fn tiny_examples() {
        let mut bs: Vec<Bridge> = vec![];
        let mut w = 1;
        let mut e = 1;
        let mut ans = 0;

        // Empty list of bridges
        {
            test_build_single(w, e, bs.as_slice(), ans,
                "No bridges");
        }

        // 1 bridge
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 2, 13 ));
            ans = 13;
            test_build_single(w, e, bs.as_slice(), ans,
                "1 bridge");
        }

        // 2 bridges, non-crossing
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 0, 6 ));
            bs.push(Bridge::new( 3, 4, 5 ));
            ans = 11;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, non-crossing");
        }

        // 2 bridges, common west city, N is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 0, 8 ));
            bs.push(Bridge::new( 2, 3, 4 ));
            ans = 8;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common west city, N is best");
        }

        // 2 bridges, common west city, N is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 3, 4 ));
            bs.push(Bridge::new( 2, 0, 8 ));
            ans = 8;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common west city, N is best (reordered)");
        }

        // 2 bridges, common west city, S is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 0, 2 ));
            bs.push(Bridge::new( 2, 3, 6 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common west city, S is best");
        }

        // 2 bridges, common west city, S is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 3, 6 ));
            bs.push(Bridge::new( 2, 0, 2 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common west city, S is best (reordered)");
        }

        // 2 bridges, common east city, N is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 1, 3, 5 ));
            bs.push(Bridge::new( 2, 3, 2 ));
            ans = 5;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common east city, N is best");
        }

        // 2 bridges, common east city, N is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 3, 2 ));
            bs.push(Bridge::new( 1, 3, 5 ));
            ans = 5;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common east city, N is best (reordered)");
        }

        // 2 bridges, common east city, S is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 3, 1 ));
            bs.push(Bridge::new( 1, 3, 7 ));
            ans = 7;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common east city, S is best");
        }

        // 2 bridges, common east city, S is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 3, 1 ));
            bs.push(Bridge::new( 1, 3, 7 ));
            ans = 7;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, common east city, S is best (reordered)");
        }

        // 2 bridges, identical
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 2, 5 ));
            bs.push(Bridge::new( 0, 2, 5 ));
            ans = 5;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, identical");
        }

        // 2 bridges, same cities, different tolls (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 2, 5 ));
            bs.push(Bridge::new( 0, 2, 6 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, same cities, different tolls (reordered)");
        }

        // 2 bridges, same cities, different tolls
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 2, 6 ));
            bs.push(Bridge::new( 0, 2, 5 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, same cities, different tolls");
        }

        // 2 bridges, crossing, NW-SE is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 1, 6 ));
            bs.push(Bridge::new( 2, 0, 5 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, crossing, NW-SE is best");
        }

        // 2 bridges, crossing, NW-SE is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 0, 5 ));
            bs.push(Bridge::new( 0, 1, 6 ));
            ans = 6;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, crossing, NW-SE is best (reordered)");
        }

        // 2 bridges, crossing, SW-NE is best
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 0, 1, 5 ));
            bs.push(Bridge::new( 2, 0, 7 ));
            ans = 7;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, crossing, SW-NE is best");
        }

        // 2 bridges, crossing, SW-NE is best (reordered)
        {
            w = 5;
            e = 5;
            bs.clear();
            bs.push(Bridge::new( 2, 0, 7 ));
            bs.push(Bridge::new( 0, 1, 5 ));
            ans = 7;
            test_build_single(w, e, bs.as_slice(), ans,
                "2 bridges, crossing, SW-NE is best (reordered)");
        }
    }

    #[test] fn small_examples() {
        let mut bs: Vec<Bridge> = vec![];
        let mut w;
        let mut e;
        let mut ans;

        // Example #1 from assignment description
        {
            w = 3;
            e = 3;
            bs.clear();
            bs.push(Bridge::new( 0, 1, 3 ));
            bs.push(Bridge::new( 1, 1, 5 ));
            bs.push(Bridge::new( 1, 2, 4 ));
            bs.push(Bridge::new( 2, 0, 8 ));
            ans = 8;
            test_build_single(w, e, bs.as_slice(), ans,
                "Example #1 from assignment description");
        }

        // Example #2 from assignment description
        {
            w = 3;
            e = 3;
            bs.clear();
            bs.push(Bridge::new( 0, 1, 3 ));
            bs.push(Bridge::new( 1, 1, 5 ));
            bs.push(Bridge::new( 1, 2, 4 ));
            bs.push(Bridge::new( 2, 0, 8 ));
            bs.push(Bridge::new( 2, 2, 6 ));
            ans = 11;
            test_build_single(w, e, bs.as_slice(), ans,
                "Example #2 from assignment description");
        }

        // 3 cities each side, not all joinable
        {
            w = 3;
            e = 3;
            bs.clear();
            bs.push(Bridge::new( 0, 0, 4 ));
            bs.push(Bridge::new( 1, 0, 4 ));
            bs.push(Bridge::new( 2, 0, 4 ));
            bs.push(Bridge::new( 2, 1, 4 ));
            bs.push(Bridge::new( 2, 2, 4 ));
            ans = 8;
            test_build_single(w, e, bs.as_slice(), ans,
                "3 cities each side, not all joinable");
        }

        // 3 cities each side, all joinable
        {
            w = 3;
            e = 3;
            bs.clear();
            bs.push(Bridge::new( 0, 0, 4 ));
            bs.push(Bridge::new( 1, 0, 4 ));
            bs.push(Bridge::new( 2, 0, 4 ));
            bs.push(Bridge::new( 2, 1, 4 ));
            bs.push(Bridge::new( 2, 2, 4 ));
            bs.push(Bridge::new( 1, 1, 4 ));
            ans = 12;
            test_build_single(w, e, bs.as_slice(), ans,
                "3 cities each side, all joinable");
        }
    }

    #[test] fn medium_examples() {
        let mut bs: Vec<Bridge> = vec![];
        let mut w = 4;
        let mut e = 4;
        let mut ans = 4;

        // Greedy fails
        {
            bs.clear();
            bs.push(Bridge::new(  0,  1,  1 ));
            bs.push(Bridge::new(  1,  0,  2 ));
            bs.push(Bridge::new(  2,  3,  1 ));
            bs.push(Bridge::new(  3,  2,  2 ));
            test_build_single(w, e, bs.as_slice(), ans,
                              "Greedy fails");
        }

        // Greedy fails large
        {
            w = 20;
            e = w;
            bs.clear();
            let phi = (1f64 + 5f64.sqrt()) / 2f64;
            let mut phipower = phi;
            for i in 0..w/2 {
                if 1f64 % phipower > 0.5 {
                    bs.push(Bridge::new( 2*i,  2*i+1,  1 ));
                    bs.push(Bridge::new( 2*i+1,  2*i,  2 ));
                } else {
                    bs.push(Bridge::new( 2*i,  2*i+1,  2 ));
                    bs.push(Bridge::new( 2*i+1,  2*i,  1 ));
                }
                phipower *= phi;
            }
            ans = w as i32;
            test_build_single(w, e, bs.as_slice(), ans,
                              "Greedy fails large");
        }

        // All bridges available #1
        {
            w = 4;
            e = w;
            bs.clear();
            for i in 0..w {
                for j in 0..e {
                    bs.push(Bridge::new( i, j, 3 ));
                }
            }

            ans = w as i32 * 3;
            test_build_single(w, e, bs.as_slice(), ans,
                "All bridges available #1");
        }

        // All bridges available #2
        {
            w = 4;
            e = w;
            bs.clear();

            for i in 0..w {
                for j in 0..e {
                    bs.push(Bridge::new( i, j, if i == j { 3 } else { 5} ));
                }
            }
            ans = (w as i32 - 1) * 5;
            test_build_single(w, e, bs.as_slice(), ans,
                "All bridges available #2");
        }

        // All bridges available #3
        {
            w = 4;
            e = w;
            bs.clear();
            for i in (0..w).rev() {
                for j in (0..e).rev() {
                    bs.push(
                        Bridge::new( i, j,
                            if -1 <= i as i64 - j as i64 && i as i64 - j as i64 <= 1 { 3 } else { 7 } )
                    );
                }
            }
            ans = (w as i32 - 2) * 7;
            test_build_single(w, e, bs.as_slice(), ans,
                "All bridges available #3");
        }

        // Random #1
        {
            w = 12;
            e = 12;
            bs.clear();
            bs.push(Bridge::new(  2, 10,  5 ));
            bs.push(Bridge::new(  4,  0,  5 ));
            bs.push(Bridge::new(  1,  1,  7 ));
            bs.push(Bridge::new( 11,  2,  7 ));
            bs.push(Bridge::new(  2,  6,  5 ));
            bs.push(Bridge::new(  0,  5,  7 ));
            bs.push(Bridge::new(  7,  8,  5 ));
            bs.push(Bridge::new( 10,  1,  5 ));
            bs.push(Bridge::new(  8, 11,  7 ));
            bs.push(Bridge::new(  9,  3,  5 ));
            bs.push(Bridge::new(  3,  7,  7 ));
            bs.push(Bridge::new(  5,  4,  7 ));
            bs.push(Bridge::new(  8,  9,  7 ));
            bs.push(Bridge::new(  3,  7,  5 ));
            bs.push(Bridge::new(  6, 11,  5 ));
            bs.push(Bridge::new(  9,  4,  7 ));
            ans = 31;
            test_build_single(w, e, bs.as_slice(), ans,
                "Random #1");
        }

        // Random #2
        {
            w = 12;
            e = 12;
            bs.clear();
            bs.push(Bridge::new(  4,  8,  2 ));
            bs.push(Bridge::new( 11,  6,  2 ));
            bs.push(Bridge::new(  2, 11,  4 ));
            bs.push(Bridge::new(  5,  7,  2 ));
            bs.push(Bridge::new(  9,  1,  4 ));
            bs.push(Bridge::new(  6,  4,  2 ));
            bs.push(Bridge::new(  0,  1,  4 ));
            bs.push(Bridge::new(  3,  5,  4 ));
            bs.push(Bridge::new(  4,  9,  4 ));
            bs.push(Bridge::new(  3, 10,  2 ));
            bs.push(Bridge::new(  8,  9,  2 ));
            bs.push(Bridge::new(  3,  2,  4 ));
            bs.push(Bridge::new(  8, 11,  2 ));
            bs.push(Bridge::new( 10,  2,  4 ));
            bs.push(Bridge::new(  1,  0,  2 ));
            bs.push(Bridge::new(  7,  7,  4 ));
            ans = 16;
            test_build_single(w, e, bs.as_slice(), ans,
                "Random #2");
        }

        // Random #3
        {
            w = 12;
            e = 12;
            bs.clear();
            bs.push(Bridge::new(  7, 11,  6 ));
            bs.push(Bridge::new(  9,  4,  7 ));
            bs.push(Bridge::new(  6,  9,  7 ));
            bs.push(Bridge::new(  8,  1,  6 ));
            bs.push(Bridge::new(  0,  7,  6 ));
            bs.push(Bridge::new(  3,  6,  7 ));
            bs.push(Bridge::new(  5,  3,  6 ));
            bs.push(Bridge::new( 11,  2,  7 ));
            bs.push(Bridge::new( 10,  0,  7 ));
            bs.push(Bridge::new(  2,  7,  7 ));
            bs.push(Bridge::new(  1, 10,  7 ));
            bs.push(Bridge::new(  4,  9,  6 ));
            bs.push(Bridge::new(  5,  1,  6 ));
            bs.push(Bridge::new(  4,  2,  7 ));
            bs.push(Bridge::new(  8, 11,  6 ));
            bs.push(Bridge::new(  3,  8,  6 ));
            ans = 26;
            test_build_single(w, e, bs.as_slice(), ans,
                "Random #3");
        }

        // One bridge crosses many #1
        {
            w = 12;
            e = w;
            bs.clear();
            for i in 0..w {
                bs.push(Bridge::new( i, i, 1 ));
            }
            bs.push(Bridge::new( 0, e-1, w as i32 + 1 ));
            bs.push(Bridge::new( w-1, 0, w as i32 + 2 ));

            ans = w as i32 + 2;
            test_build_single(w, e, bs.as_slice(), ans,
                "One bridge crosses many #1");
        }

        // One bridge crosses many #2
        {
            w = 12;
            e = w;
            bs.clear();
            for i in 0..w {
                bs.push(Bridge::new( i, i, 1 ));
            }
            bs.push(Bridge::new( 0, e-1, w as i32 - 1 ));
            bs.push(Bridge::new( w-1, 0, w as i32 - 2 ));

            ans = w as i32;
            test_build_single(w, e, bs.as_slice(), ans,
                "One bridge crosses many #2");
        }
    }
}
